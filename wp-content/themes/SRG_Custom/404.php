<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>
    <?php /* Translation friendly functions: _e('Text to translate', 'theme-slug') */ ?>
	<?php _e( 'Not Found', 'srg' ); ?>
	<?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'srg' ); ?>
<?php get_footer(); ?>