<?php
/**
 * The main template file.
 * Default page layout, uses for listing 'Blog Page' and defaults to home page if none other is set in WP Reading Settings
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
get_header(); ?>

			<?php
			/* Run the loop to output the posts. */
			 get_template_part( 'loop', 'index' );
			?>

<?php get_footer(); ?>