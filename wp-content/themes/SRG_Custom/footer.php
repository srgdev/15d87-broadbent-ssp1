<?php
/**
 * The template for displaying the footer.
 * Closes our page and calls wp_footer() to add js and other options
 * @uses wp_footer()
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
?>

<?php wp_footer(); ?>
</body>
</html>