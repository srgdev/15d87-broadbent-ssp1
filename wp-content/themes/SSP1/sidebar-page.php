<div id="sidebar" class="homeSidebar">
	<?php if(!dynamic_sidebar('home-box')): ?>
    <div class="sideItem signup">
    	<h1>GET INVOLVED</h1>
        <form method="post" action="#" data-validate="parsley">
            <input name="name" type="text" placeholder="Name" class="field" data-required="true">
            <input name="email" type="text" placeholder="Email" class="field" data-required="true" data-type="email">
            <input name="submit" type="submit" value="SUBMIT" class="submit bgcolor-tertiary">
        </form>
    </div>
    
    <div class="sideItem events">
   		<h1>EVENTS</h1>
        <div class="event">
        	<div class="eventDate"><i class="fa fa-clock-o"></i> March 15, 2014</div>
            <h3>Lorem ipsum dolor sit amet, consectetuer</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <a href="#">[…]</a></p>
        </div>
        <div class="event">
        	<div class="eventDate"><i class="fa fa-clock-o"></i> March 15, 2014</div>
            <h3>Lorem ipsum dolor sit amet, consectetuer</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <a href="#">[…]</a></p>
        </div>
    </div>
    
    <div class="sideItem socWidget">
    	<iframe src="https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FFacebookDevelopers&amp;width&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=628393553876847" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>
    </div>
    <?php endif; ?>
    
</div><!-- End Sidebar-->