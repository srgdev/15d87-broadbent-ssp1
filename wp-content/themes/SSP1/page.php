<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

<div id="contentRow" class="row">
    <div class="rowInner <?php echo is_active_sidebar('home-box') ? 'hasSidebar' : ''; ?>">
    
        <?php if(is_active_sidebar('home-box')): ?>
            <?php get_sidebar('page'); ?>
        <?php endif; ?>
        
        <div id="content">
            <?php if(have_posts()): while(have_posts()): the_post(); ?>
                <h1><?php echo strtoupper(get_the_title()); ?></h1>
                <?php if(has_post_thumbnail()): ?>
                    <div class="mainPic"><?php the_post_thumbnail(); ?></div>
                <?php endif; ?>
                <?php the_content(); ?>
            <?php endwhile; endif; ?>
        </div> <!-- End Content -->
    
        <br class="clear">
    
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>