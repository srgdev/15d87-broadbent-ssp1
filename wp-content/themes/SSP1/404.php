<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>

<div id="contentRow" class="row">
    <div class="rowInner">

        <div id="content">
                <h1>404 Page Not Found</h1>
                <p>The page you requested could not be found.</p>
        </div> <!-- End Content -->
    
        <br class="clear">
    
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>