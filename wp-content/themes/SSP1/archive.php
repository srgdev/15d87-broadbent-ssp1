<?php
/**
 * The template for displaying Archive pages.
 *
 */

get_header(); ?>

<div id="contentRow" class="row">
    <div class="rowInner <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
    
        <?php if(is_active_sidebar('blog-sidebar')): ?>
            <?php get_sidebar('blog'); ?>
        <?php endif; ?>
        
        <div id="content">
           <?php get_template_part('loop'); ?>
        </div> <!-- End Content -->
    
        <br class="clear">
    
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>