<?php
/**
 * Template Name: Extended
 */

get_header(); ?>

<div id="contentRow" class="row">
    <div class="rowInner">

        <div id="content">
            <?php 
                $id=58; 
                $post = get_post($id); 
                $content = apply_filters('the_content', $post->post_content);
                $array = get_extended($content);
                echo apply_filters('the_content', $array['main']);
            ?>
        </div> <!-- End Content -->
    
        <br class="clear">
    
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>