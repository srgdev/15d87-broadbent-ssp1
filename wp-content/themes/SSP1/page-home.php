<?php
/**
 * Template Name: Home page, sidebar
 *
 */

get_header(); ?>

<div id="contentRow" class="row">
    <div class="rowInner <?php echo is_active_sidebar('home-box') ? 'hasSidebar' : ''; ?>">
    
        <?php if(is_active_sidebar('home-box')): ?>
            <?php get_sidebar('page'); ?>
        <?php endif; ?>
        
        <div id="content">
           <?php get_template_part('loop', 'home'); ?>
        </div> <!-- End Content -->
    
        <br class="clear">
    
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>