<?php
// Include our widget files here
include(TEMPLATEPATH . '/includes/widgets/srg_firstclass_widget.php');
include(TEMPLATEPATH . '/includes/widgets/srg_facebook_widget.php');
include(TEMPLATEPATH . '/includes/widgets/srg_search_widget.php');
include(TEMPLATEPATH . '/includes/widgets/srg_category_widget.php');
include(TEMPLATEPATH . '/includes/widgets/srg_tag_widget.php');
include(TEMPLATEPATH . '/includes/widgets/srg_archive_widget.php');
include(TEMPLATEPATH . '/includes/widgets/srg_events_widget.php');

// Include CreateSendAPI wrapper
require_once TEMPLATEPATH.'/includes/createsend/class/base_classes.php';
if (!class_exists('Services_JSON')) {
	require_once TEMPLATEPATH.'/includes/createsend/class/services_json.php';
}
require_once TEMPLATEPATH.'/includes/createsend/class/serialisation.php';
require_once TEMPLATEPATH.'/includes/createsend/class/transport.php';
require_once TEMPLATEPATH.'/includes/createsend/class/log.php';
require_once TEMPLATEPATH.'/includes/createsend/csrest_lists.php';
require_once TEMPLATEPATH.'/includes/createsend/csrest_subscribers.php';

// unregister all widgets
function unregister_default_widgets() {
	unregister_widget('WP_Widget_Pages');
	unregister_widget('WP_Widget_Calendar');
	unregister_widget('WP_Widget_Archives');
	unregister_widget('WP_Widget_Links');
	unregister_widget('WP_Widget_Meta');
	unregister_widget('WP_Widget_Search');
	unregister_widget('WP_Widget_Text');
	unregister_widget('WP_Widget_Categories');
	unregister_widget('WP_Widget_Recent_Posts');
	unregister_widget('WP_Widget_Recent_Comments');
	unregister_widget('WP_Widget_RSS');
	unregister_widget('WP_Widget_Tag_Cloud');
	unregister_widget('WP_Nav_Menu_Widget');
	unregister_widget('Twenty_Eleven_Ephemera_Widget');
}
add_action('widgets_init', 'unregister_default_widgets', 11);