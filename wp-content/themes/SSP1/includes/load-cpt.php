<?php
/**
* Add custom post types and support for custom post type taxonomies, archives, and rewrite rules
* @uses register_taxonomy()
* @uses register_post_type()
* @hook generate_rewrite_rules
* @
*/

add_action('init', 'srg_register_post_type');

function srg_register_post_type() {

	register_post_type('event', array(
	'labels' => array(
		'name' => 'Events',
		'singular_name' => 'Event',
		'add_new' => 'Add new Event',
		'add_new_item'=> 'Add new Event',
		'edit_item' => 'Edit Event',
		'new_item' => 'New Event',
		'view_item' => 'View Event',
		'search_items' => 'Search Events',
		'not_found' => 'No events found',
		'not_found_in_trash' => 'No events found in Trash'
	),
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'taxonomies' => array('event_category', 'event_tag'),
	'supports' => array( 'title', 'editor', 'author', 'thumbnail')
    ));

	register_post_type('slide', array(
	'labels' => array(
		'name' => 'Slides',
		'singular_name' => 'Slide',
		'add_new' => 'Add new Slide',
		'add_new_item'=> 'Add new Slide',
		'edit_item' => 'Edit Slide',
		'new_item' => 'New Slide',
		'view_item' => 'View Slide',
		'search_items' => 'Search Slides',
		'not_found' => 'No Slides found',
		'not_found_in_trash' => 'No Slides found in Trash'
	),
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'supports' => array( 'title', 'editor', 'author', 'thumbnail')
    ));

	// Add Event Category Taxonomy
	$EClabels = array(
		'name'                       => _x( ' Event Categories', 'taxonomy general name' ),
		'singular_name'              => _x( ' Event Category', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Event Categories' ),
		'popular_items'              => __( 'Popular Event Categories' ),
		'all_items'                  => __( 'All Event Categories' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Event Category' ),
		'update_item'                => __( 'Update Event Category' ),
		'add_new_item'               => __( 'Add New Event Category' ),
		'new_item_name'              => __( 'New Event Category Name' ),
		'separate_items_with_commas' => __( 'Separate event categories with commas' ),
		'add_or_remove_items'        => __( 'Add or remove event categories' ),
		'choose_from_most_used'      => __( 'Choose from the most used event categories' ),
		'not_found'                  => __( 'No event categories found.' ),
		'menu_name'                  => __( 'Event Categories' ),
	);

	$ECargs = array(
		'hierarchical'          => false,
		'labels'                => $EClabels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'event-category' ),
	);

	register_taxonomy( 'event_category', 'event', $ECargs );

	// Add Event Tag Taxonomy
	$ETlabels = array(
		'name'                       => _x( ' Event Tags', 'taxonomy general name' ),
		'singular_name'              => _x( ' Event Tag', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Event Tags' ),
		'popular_items'              => __( 'Popular Event Tags' ),
		'all_items'                  => __( 'All Event Tags' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Event Tag' ),
		'update_item'                => __( 'Update Event Tag' ),
		'add_new_item'               => __( 'Add New Event Tag' ),
		'new_item_name'              => __( 'New Event Tag Name' ),
		'separate_items_with_commas' => __( 'Separate event tags with commas' ),
		'add_or_remove_items'        => __( 'Add or remove event tags' ),
		'choose_from_most_used'      => __( 'Choose from the most used event tags' ),
		'not_found'                  => __( 'No event tags found.' ),
		'menu_name'                  => __( 'Events Tags' ),
	);

	$ETargs = array(
		'hierarchical'          => false,
		'labels'                => $ETlabels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'event-tag' ),
	);

	register_taxonomy( 'event_tag', 'event', $ETargs );
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Add rewrite rules for "event" post type
* @param object $wp_rewrite
*/
function event_rewrite_rules($wp_rewrite) {
    $rules = cpt_generate_date_archives('event', $wp_rewrite);
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
    return $wp_rewrite;
}
add_action('generate_rewrite_rules', 'event_rewrite_rules');

/*----------------------------------------------------------------------------------------------------*/

/**
* Generate date archive rewrite rules for a given custom post type
* @param string $cpt slug of the custom post type
* @param object $wp_rewrite object containing rules/methods for url rewrites
*
* @return array $rules returns a set of rewrite rules for Wordpress to handle
*/
function cpt_generate_date_archives($cpt, $wp_rewrite) {
    $rules = array();

    $post_type = get_post_type_object($cpt);
    $slug_archive = $post_type->has_archive;
    if ($slug_archive === false) return $rules;
    if ($slug_archive === true) {
        $slug_archive = $post_type->name;
    }

    $dates = array(
        array(
            'rule' => "([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})",
            'vars' => array('year', 'monthnum', 'day')),
        array(
            'rule' => "([0-9]{4})/([0-9]{1,2})",
            'vars' => array('year', 'monthnum')),
        array(
            'rule' => "([0-9]{4})",
            'vars' => array('year'))
        );

    foreach ($dates as $data) {
        $query = 'index.php?post_type='.$cpt;
        $rule = $slug_archive.'/'.$data['rule'];

        $i = 1;
        foreach ($data['vars'] as $var) {
            $query.= '&'.$var.'='.$wp_rewrite->preg_index($i);
            $i++;
        }

        $rules[$rule."/?$"] = $query;
        $rules[$rule."/feed/(feed|rdf|rss|rss2|atom)/?$"] = $query."&feed=".$wp_rewrite->preg_index($i);
        $rules[$rule."/(feed|rdf|rss|rss2|atom)/?$"] = $query."&feed=".$wp_rewrite->preg_index($i);
        $rules[$rule."/page/([0-9]{1,})/?$"] = $query."&paged=".$wp_rewrite->preg_index($i);
    }
    return $rules;
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Filter main query to order events by meta key/val for start_date
 * @param object $query global wp_query
 */
function srg_sort_events($query){

	if(is_post_type_archive('event') && $query->is_main_query()) {

		$query->set('meta_key', 'start_date');
		$query->set('orderby',  'meta_value_num');
		$query->set('order',  'DESC');

	}

}
add_action('pre_get_posts', 'srg_sort_events');
