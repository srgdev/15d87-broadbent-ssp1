<?php
/**
 * Add some custom options to the customize panel
 *
 *@uses $wp_customize object for setting Theme Options panel sections
 *@uses add_setting()
 *@uses add_section()
 *@uses add_control()
 *
 */
function srg_customize_register($wp_customize) {

    require_once(TEMPLATEPATH . '/includes/classes/WP_Customize_Control_Textarea.php');

	// Theme logos
	$wp_customize->add_setting( 'srg_theme_logo', array('default' => false,'type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_theme_logo_footer', array(	'default' => false,'type' => 'theme_mod') );
	// Theme background images
	$wp_customize->add_setting( 'srg_theme_header_bg', array(	'default' => false,'type' => 'theme_mod') );
	// Theme background colors
	$wp_customize->add_setting( 'srg_bg_color_primary', array('default' => '#012442','type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_bg_color_secondary', array('default' => '#ff000e',	'type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_bg_color_tertiary', array('default' => '#00688d','type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_slider_bg', array('default' => '#ffffff','type' => 'theme_mod') );
	// Theme text colors
	$wp_customize->add_setting( 'srg_theme_text_primary_color', array('default' => '#98012e','type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_theme_text_secondary_color', array('default' => '#ff000e','type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_theme_text_tertiary_color', array(	'default' => '#00688d','type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_theme_text_body_color', array(	'default' => '#898989','type' => 'theme_mod') );
	// Slider options
	$wp_customize->add_setting( 'srg_theme_showslider', array(	'default' => '1','type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_theme_numslides', array(	'default' => '3','type' => 'theme_mod') );
	// Social media
	$wp_customize->add_setting( 'srg_theme_twitter', array(	'default' => false,'type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_theme_facebook', array('default' => false,'type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_theme_youtube', array(	'default' => false,'type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_theme_instagram', array(	'default' => false,'type' => 'theme_mod') );
	// Donate link
	$wp_customize->add_setting( 'srg_theme_donatelink', array(	'default' => false,'type' => 'theme_mod') );
	$wp_customize->add_setting( 'srg_theme_donatetext', array(	'default' => false,'type' => 'theme_mod') );
	// Paid for by... info
	$wp_customize->add_setting( 'srg_theme_disclaimer', array(	'default' => false,'type' => 'theme_mod') );
    // Analytics
    $wp_customize->add_setting( 'srg_theme_analytics', array(	'default' => false,'type' => 'theme_mod') );

	// Add options sections
	$wp_customize->add_section( 'srg_header_options', array('title' => 'Header Logo','priority' => 35) );
	$wp_customize->add_section( 'srg_banner_options', array('title' => 'Slider Options','priority' => 36) );
	$wp_customize->add_section( 'srg_social_options', array('title' => 'Social/General Options','priority' => 37) );
	$wp_customize->add_section( 'srg_color_options', array(	'title' => 'Background Colors',	'priority' => 38) );
	$wp_customize->add_section( 'srg_text_options', array(	'title' => 'Text/Link Colors',	'priority' => 39) );
	$wp_customize->add_section( 'srg_footer_options', array('title' => 'Footer Options','priority' => 40) );

	// Add controls to our options sections
	// Header and Footer Images
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_image', array('label'   => 'Header Image','section' => 'srg_header_options',	'settings'   => 'srg_theme_logo',) ) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_bg_image', array('label'   => 'Header Background Image','section' => 'srg_header_options',	'settings'   => 'srg_theme_header_bg',) ) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_image', array('label'   => 'Footer Image','section' => 'srg_footer_options',	'settings'   => 'srg_theme_logo_footer',) ) );
	// Colors
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'primary_color', array('label'   => 'Primary Color',	'section' => 'srg_color_options','settings'   => 'srg_bg_color_primary',) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'secondary_color', array('label'   =>  'Secondary Color','section' => 'srg_color_options','settings'   => 'srg_bg_color_secondary',) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_background_color', array('label'   => 'Tertiary Background Color',	'section' => 'srg_color_options','settings'   => 'srg_bg_color_tertiary',) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'banner_background_color', array('label'   => 'Slider Background Color',	'section' => 'srg_banner_options','settings'   => 'srg_slider_bg',) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'srg_text_primary_color', array('label'   => 'Primary Text Color','section' => 'srg_text_options','settings'   => 'srg_theme_text_primary_color',) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'txt_secondary_color', array('label'   => 'Secondary Text Color','section' => 'srg_text_options','settings'   => 'srg_theme_text_secondary_color',) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'txt_tertiary_color', array(	'label'   => 'Tertiary Text Color',	'section' => 'srg_text_options','settings'   => 'srg_theme_text_tertiary_color',) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'txt_body_color', array(	'label'   => 'Body Text Color',	'section' => 'srg_text_options','settings'   => 'srg_theme_text_body_color',) ) );
	// Slider opts
	$wp_customize->add_control( 'srg_theme_numslides', array('label' => __( 'Number of slides' ),'section' => 'srg_banner_options',) );
	$wp_customize->add_control( new WP_Customize_Control($wp_customize,'showslider', array('label' => __( 'Show the slider?', 'srg' ),'section'=> 'srg_banner_options','settings' => 'srg_theme_showslider','type' => 'checkbox') ) );
	// Social Media
	$wp_customize->add_control( 'srg_theme_twitter', array('label' => __( 'Twitter Handle/Username' ),'section' => 'srg_social_options',) );
	$wp_customize->add_control( 'srg_theme_facebook', array('label' => __( 'Facebook page URL' ),'section' => 'srg_social_options',) );
	$wp_customize->add_control( 'srg_theme_youtube', array('label' => __( 'Youtube Username' ),'section' => 'srg_social_options',) );
	$wp_customize->add_control( 'srg_theme_instagram', array('label' => __( 'Instagram Username' ),'section' => 'srg_social_options',) );
	// Donate link
	$wp_customize->add_control( 'srg_theme_donatelink', array('label' => __( 'Donate button link' ),'section' => 'srg_social_options',) );
	$wp_customize->add_control( 'srg_theme_donatetext', array('label' => __( 'Donate button text' ),'section' => 'srg_social_options',) );
	// Paid for by
	$wp_customize->add_control( 'srg_theme_disclaimer', array('label' => __( '"Paid for by" text' ),'section' => 'srg_footer_options',) );
    // Analytics
    $wp_customize->add_control( new WP_Customize_Control_Textarea( $wp_customize, 'srg_theme_analytics', array('label' => __( 'Google Analytics Code'), 'section' => 'srg_social_options', 'settings' => 'srg_theme_analytics')));
}
add_action( 'customize_register', 'srg_customize_register' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Check options that contain urls and ensure they are on the local domain
 */
function check_options_urls(){

    $current_url = get_bloginfo('url');

    $opts = array();
    $opts['srg_theme_logo'] = get_theme_mod('srg_theme_logo');
    $opts['srg_theme_logo_footer'] = get_theme_mod('srg_theme_logo_footer');
    $opts['srg_theme_header_bg'] = get_theme_mod('srg_theme_header_bg');

    foreach($opts as $name=>$opt){
        if($opt != ''){
            if(strpos($opt, $current_url) < 0){
                $path =  substr($opt, -(strlen($opt) - strpos($opt, '/wp-content/')), (strlen($opt) - strpos($opt, '/wp-content/')));
                set_theme_mod($name, $current_url . $path);
            }
        }
    }
}
add_action('init', 'check_options_urls');