<?php
/**
* Include createsend API wrapper
*/
require_once TEMPLATEPATH.'/includes/createsend/class/base_classes.php';
if (!class_exists('Services_JSON')) {
	require_once TEMPLATEPATH.'/includes/createsend/class/services_json.php';
}
require_once TEMPLATEPATH.'/includes/createsend/class/serialisation.php';
require_once TEMPLATEPATH.'/includes/createsend/class/transport.php';
require_once TEMPLATEPATH.'/includes/createsend/class/log.php';
require_once TEMPLATEPATH.'/includes/createsend/csrest_general.php';
require_once TEMPLATEPATH.'/includes/createsend/csrest_lists.php';
require_once TEMPLATEPATH.'/includes/createsend/csrest_subscribers.php';
require_once TEMPLATEPATH.'/includes/createsend/csrest_clients.php';
	
/*----------------------------------------------------------------------------------------------------*/

/**
* Register options array for our FirstClass account, forms, lists, etc
*/
function register_firstclass_settings() {
	register_setting('srg-firstclass-settings', 'srg-firstclass-settings');
	register_setting('srg-firstclass-storedAPI', 'srg-firstclass-storedAPI');
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Create an administration page to handle entry of API data and selecting lists
* 
*/
function firstclass_settings_page() {

    $options = get_option('srg-firstclass-settings');
    $stored = get_option('srg-firstclassed-storedAPI');

    if(current_user_can('administrator')): ?>
		
		<div class="wrap firstclass">
			<h2>FirstClass Form Integration</h2>
			<div id="saved"></div>
			
				<?php if($options['clientID']): $class1 = 'hidden'?><?php endif; ?>
				<div id="step1wrapper" class="<?php echo $class1; ?>">
				    <p>Please follow the instructions to enable FirstClass integration into this theme.</p>
				    <form method="post" action="options.php" id="step1Form">
        				<h3>Step 1: Select or add a client</h3>
        				<p>Select client or type in a name to create a new client.</p>
        				<select id="client" name="clientID">
        					<?php client_list(); ?>
        				</select>
        				<input type="text" name="clientName" id="clientName" />
        				<input type="hidden" name="step" id="step" value="1" />
        				<input type="hidden" name="action" id="action" value="firstclass_handler" />
        				<input class="button button-primary" type="submit" id="step1" value="Next" />
    				</form>
				</div>

                <?php if(!$options['clientID'] || $options['pageId']): $class2 = 'hidden'?><?php endif; ?>
				<div id="step2wrapper"  class="<?php echo $class2; ?>">
				    <p>Please follow the instructions to enable FirstClass integration into this theme.</p>
				    <form method="post" action="options.php" id="step2Form">
        				<h3>Step 2: Select thankyou page.</h3>
        				<p>If you have not created a thankyou page, save your progress here and create your page.  When you return, you can select it.</p>
        				<label for="emailerThanks">Emailer signup: </label><?php wp_dropdown_pages(array('name' => 'pageId', 'id' => 'emailerThanks')); ?>
        				<input type="hidden" name="action" id="action" value="firstclass_handler" />
        				<input type="hidden" name="step" value="2" />
        				<input class="button button-primary" type="submit" id="step2" value="Next" />
    				</form>
				</div>
				
				
				<?php if(!$options['clientID'] || !$options['listId'] || !$options['pageId']): $class3 = 'hidden'?><?php endif; ?>
				<div id="complete" class="<?php echo $class3; ?>">
				    <p>Client: <?php echo $options['clientName']; ?></p>
				    <p>List: Website Email Signup (SRG): <?php echo $options['listId']; ?></p>
				    <p>Thankyou Page: <?php echo get_the_title($options['pageId']); ?></p>
				    <input class="button button-primary" type="submit" id="reset" value="Reset" />
				</div>
				
			</form>
		</div>
	<?php endif;
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Add hooks for registering settings and settings page
*/
function srg_create_menu() {
	if ( isset( $_GET['page'] ) && $_GET['page'] == 'firstclass_settings' ) {
		add_action( 'admin_enqueue_scripts', 'firstclass_enqueue' );
    }
	add_theme_page('FirstClass Form Settings', 'FirstClass Form Settings', 'manage_options', 'firstclass_settings', 'firstclass_settings_page');
	add_action( 'admin_init', 'register_firstclass_settings' );
}
add_action('admin_menu', 'srg_create_menu');

/*----------------------------------------------------------------------------------------------------*/

/**
* Enqueue styles, scripts for admin page, register ajax handler
*/
function firstclass_enqueue() { 
	wp_enqueue_script( 'firstclass-settings-script', get_template_directory_uri().'/js/firstclassadmin.js', array('jquery') );
	wp_enqueue_style( 'firstclass_style', get_template_directory_uri().'/css/firstclassadmin.css');
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Process saved form information
* @uses $_POST global
* @uses get_option()
* @uses create_client()
* @uses create_list()
*/
add_action('wp_ajax_firstclass_handler', 'firstclass_handler');

function firstclass_handler() {
    // parse $_POST array
    $data = $_POST;
    unset($data['security'], $data['action']);
    // If RESET, do it and die
    if($data['reset']){
        delete_option('srg-firstclass-settings');
        die('1');
    } else {
    	// Load options array
    	if(!is_array(get_option('srg-firstclass-settings'))) {
            $options = array();
        } else {
            $options = get_option('srg-firstclass-settings');
        }
        if($data['step'] == '1'):
    	   $options = set_client($options, $data);
            $options = set_list($options, $data);
        endif;
        if($data['step'] == '2'):
        $options = set_thankpage($options, $data);
        endif;
        // No faults?
        if($options){
            if(update_option('srg-firstclass-settings', $options)) {
                die('1');
            } else {
                die('0');
            }
        }
    }   
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Process signup form information on frontend - adds a nopriv function call and a frontend localization for our main JS
* @uses $_POST global
*/
add_action('wp_ajax_firstclass_signup', 'firstclass_signup');
add_action( 'wp_ajax_nopriv_firstclass_signup', 'firstclass_signup' );

function firstclass_signup() {
    // parse $_POST array
    $data = $_POST;
    // store all our values
    $name = strip_tags($data['name']) ? strip_tags($data['name']) : NULL;
    $email = strip_tags($data['email']) ? strip_tags($data['email']) : NULL;
    $listID = get_option('srg-firstclass-settings');
    $listID = $listID['listId'];
    
    if($name && $email && $listID) {
        $auth = array('api_key' => 'bb6d4dc56e8006e41a8b7be1c86fc898');
        $wrap = new CS_REST_Subscribers($listID, $auth);
        $result = $wrap->add(array(
            'EmailAddress' => $email,
            'Name' => $name,
            'CustomFields' => array(
                array(
                    'Key' => 'ZIP',
                    'Value' => $zip
                )
            ),
            'Resubscribe' => true
        ));
        if($result->was_successful()) {
            die('1');
        } else {
            die('0');
        }
    } else {
        die('0');
    } 
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Creates a new list when a CreateSend Client ID is entered
* @param string $listName name of list to be created
* @param string $clientID client ID to assign list to
* @param string $pageID numeric page id used as thankyou page
* 
* @return string $listID id of list created, or false if could not proceed
*/
function create_list($clientID) {
	if($clientID) {
		$auth = array('api_key' => 'bb6d4dc56e8006e41a8b7be1c86fc898');
		$wrap = new CS_REST_Lists(NULL, $auth);
		$result = $wrap->create($clientID, array(
			'Title' => 'Website Email Signup (SRG)',
			'UnsubscribePage' => '',
			'ConfirmedOptIn' => false,
			'ConfirmationSuccessPage' => '',
			'UnsubscribeSetting' => CS_REST_LIST_UNSUBSCRIBE_SETTING_ALL_CLIENT_LISTS
		));
		if($result->was_successful()) {
			$listID = $result->response;
			$list = new CS_REST_Lists($listID, $auth);
			$create = $list->create_custom_field(array(
				'FieldName' => 'ZIP',
				'DataType' => CS_REST_CUSTOM_FIELD_TYPE_NUMBER,
			));
			if($create->was_successful()) {
			     return $listID;
			} else {
				die ($create->response);
			}
		} else {
			die ($result->response);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/

/**
* return a list of valid emailer lists as a select option
* 
*/
function emaillists_list($clientID){
    $auth = array('api_key' => 'bb6d4dc56e8006e41a8b7be1c86fc898');
    $wrap = new CS_REST_Clients($clientID, $auth);
    $result = $wrap->get_lists();
    $return = array();
    foreach($result->response as $list){
        $return[$list->ListID] = $list->Name;
    }
    return $return;
}

/*----------------------------------------------------------------------------------------------------*/

/**
* return a list of clients as a select option
* 
*/
function client_list(){
	$auth = array('api_key' => 'bb6d4dc56e8006e41a8b7be1c86fc898');
	$wrap = new CS_REST_General($auth);
	$result = $wrap->get_clients();
	if($result->was_successful()) {
	    $clients = $result->response;
		
		foreach ($clients as $client){
			echo '<option value="'.$client->ClientID.'">'.$client->Name.'</option>';
		}
	} else {
	   echo '<option value="null">No clients could be found</option>';
	}
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Create a client
* 
*/
function update_list($listId, $pageId){
    $auth = array('api_key' => 'bb6d4dc56e8006e41a8b7be1c86fc898');
    $wrap = new CS_REST_Lists($listId, $auth);
    $page = get_permalink($pageId);
    $result = $wrap->update(array(
        'Title' => 'Website Email Signup (SRG)',
        'UnsubscribePage' => '',
        'ConfirmedOptIn' => false,
        'ConfirmationSuccessPage' => $page,
        'UnsubscribeSetting' => CS_REST_LIST_UNSUBSCRIBE_SETTING_ALL_CLIENT_LISTS,
        'AddUnsubscribesToSuppList' => true,
        'ScrubActiveWithSuppList' => true
    ));

    if($result->was_successful()) {
        return true;
    } else {
       die ($result->response);
    }
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Create a client
* 
*/
function create_client($clientName) {
	$auth = array('api_key' => 'bb6d4dc56e8006e41a8b7be1c86fc898');
	$wrap = new CS_REST_Clients(NULL, $auth);
	
	$result = $wrap->create(array(
	    'CompanyName' => $clientName,
	    'Country' => 'United States of America',
	    'Timezone' => '(GMT-05:00) Eastern Time (US & Canada)'
	));
	
	if($result->was_successful()) {
	    return $result->response;
	} else {
	   die('Unable to create Client');
	}
};

/*----------------------------------------------------------------------------------------------------*/

/**
* Handle client selection and updating
* 
*/
function set_client($options, $data){
    if($data['clientID']){
        if($data['clientID'] != $options['clientID']){
            $options['clientID'] = $data['clientID'];
            $options['clientName'] = $data['clientIDName'];
        }
        return $options;
    } elseif ($data['clientName']){
        $clientID = create_client($data['clientName']);
        if($clientID){
            $options['clientID'] = $clientID;
            $options['clientName'] = $data['clientName'];
            return $options;
        } else {
           die('Unable to create Client');
        }
    }
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Handle list selection and updating
* 
*/
function set_list($options, $data){
    $listsNames = emaillists_list($options['clientID']);
    $hasValid = false;
    if($listsNames){
        foreach ($listsNames as $id=>$name){
            if($name == 'Website Email Signup (SRG)') {
                $options['listId'] = $id;
                $options['listName'] = $name;
                $hasValid = true;
            }
        }
    }
    if(!$hasValid){
        $listId = create_list($options['clientID']);
        if($listId){
            $options['listId'] = $listId;
            return $options;
        } else {
            die('Unable to Create List');
        }
    } else {
        return $options;
    }
}

/*----------------------------------------------------------------------------------------------------*/

/**
* Handle thankyou page selection and updating
* 
*/
function set_thankpage($options, $data){
    if($options['pageId'] != $data['pageId']){
        $options['pageId'] = $data['pageId'];
    }
   if(update_list($options['listId'], $options['pageId'])){
       return $options;
   } else {
      die('Unable to update list.');
   };
}
