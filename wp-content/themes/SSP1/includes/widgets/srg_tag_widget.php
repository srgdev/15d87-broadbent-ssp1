<?php

/**
* SRG Tags Widget - Generate a list of tags for posts or custom taxonomies for custom post types
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_Tag_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_Tag_Widget', 'SRG Tag Widget', array( 'description' => 'Tag List Widget')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {

		?>
        
        <?php if(!is_front_page()): ?>
        
        	<?php if(get_post_type() == 'post'): ?>
        
            <div class="sideItem listItem linkcolor-secondary">
            	<h2><?php echo $instance['title']; ?></h2>
                <ul>
                	<?php wp_list_categories('title_li=&taxonomy=post_tag'); ?>
                </ul>
            </div>
            
            <?php else: ?>
            
              <div class="sideItem listItem linkcolor-secondary">
            	<h2><?php echo $instance['title']; ?></h2>
                <ul>
                	<?php wp_list_categories('title_li=&taxonomy=event_tag'); ?>
                </ul>
            </div>
            
            <?php endif; ?>
        
        <?php endif; ?>
        
        <?php
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = 'Tags';
		}
		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
		<?php 


	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
		
	}
	
}

// Register and load the widget
function srg_tag_widget_load() {
	register_widget( 'SRG_Tag_Widget' );
}
add_action( 'widgets_init', 'srg_tag_widget_load' );

?>