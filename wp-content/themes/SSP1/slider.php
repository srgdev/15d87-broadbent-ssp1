<?php $args = array('post_type' => 'slide', 'posts_per_page' => get_theme_mod('srg_theme_numslides')); ?>
<?php $slider = new WP_Query($args); ?>
<?php if($slider->have_posts()): ?>
    <div id="slideshow" class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="6000" data-cycle-slides=".slide" data-cycle-pause-on-hover="true">
        <div class="cycle-prev slideControl"><i class="fa fa-chevron-left"></i></div>
    	<div class="cycle-next slideControl"><i class="fa fa-chevron-right"></i></div>
    	 <?php while($slider->have_posts()): $slider->the_post(); ?>
    		<div class="slide">
    	    	<?php if(has_post_thumbnail()): ?>
    	    		<?php the_post_thumbnail('slider'); ?>
    	    	<?php else: ?>
    	    		<img src="<?php echo get_template_directory_uri(); ?>'/images/slide1.jpg">
    	    	<?php endif; ?>
    	        <div class="content">
    	        	<h2><?php the_title(); ?></h2>
    	            <span><?php echo get_the_content(); ?></span>
    	        </div>
    	    </div>
        <?php endwhile;?>
    </div> <!-- END SLIDESHOW -->
<?php endif; ?>